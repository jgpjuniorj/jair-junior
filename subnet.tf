#SUB REDE PRIVADA
#VOU TRABALHAR COM DUAS ZONAS DE DISPONIBILIDADE. ZONA A
resource "aws_subnet" "sub_int_prod_a" {     #sub_int_prod_a É O NOME DA SUA SUBREDE, ESCOLHA UM DA SUA PREFERÊNCIA.
vpc_id     = aws_vpc.vpc_prod.id             # AQUI VOCÊ DEFINE A QUAL VPC ESSA SUBREDE SERÁ ADICIONADA. COLOQUE O NOME DA SUA VPC PRINCIPAL, NO MEU CASO, VPC_PROD.
  cidr_block = "10.110.1.0/24"               # AQUI VOCÊ DEFINE O BLOCO DE IPS DA SUA SUBREDE. ESCOLHA DE ACORDO COM A QUANTIDADE DE HOSTS Q VOCÊ VAI UTILIZAR.
  availability_zone = "us-east-1a"           # AQUI VOCÊ DEFINE A REGIÃO QUE A REDE SERÁ CRIADA.
  
  tags = {
    Name = "sub_net_int_prod_a" 
    Envolroment = "desafio-gb"             # AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
# SUB REDE INTERNA. ZONA B
resource "aws_subnet" "sub_int_prod_b" {
vpc_id     = aws_vpc.vpc_prod.id
  cidr_block = "10.110.2.0/24"
  availability_zone = "us-east-1b"           #AQUI, DEFINIMOS A REGIÃO 2 PARA CRIAR ESSA SUBREDE, POIS, O IDEAL É SEPARARMOS EM ZONAS DE DISPONIBILIDADE.
  
  tags = {
    Name = "sub_net_int_prod_b" 
    Envolroment = "desafio-gb"              #AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
#SUB REDE PUBLICA. EXTERNA B
resource "aws_subnet" "sub_ext_prod_b" {     #sub_ext_prod_a É O NOME DA SUA SUBREDE, ESCOLHA UM DA SUA PREFERÊNCIA. LEMBRANDO QUE É SUA REDE EXTERNA
vpc_id     = aws_vpc.vpc_prod.id             # AQUI VOCÊ DEFINE A QUAL VPC ESSA SUBREDE SERÁ ADICIONADA. COLOQUE O NOME DA SUA VPC PRINCIPAL, NO MEU CASO, VPC_PROD.
  cidr_block = "10.110.10.0/24"              # AQUI VOCÊ DEFINE O BLOCO DE IPS DA SUA SUBREDE. ESCOLHA DE ACORDO COM A QUANTIDADE DE HOSTS Q VOCÊ VAI UTILIZAR.
  availability_zone = "us-east-1b"           # AQUI VOCÊ DEFINE A REGIÃO QUE A REDE SERÁ CRIADA.
  
  tags = {
    Name = "sub_net_ext_prod_b" 
    Envolroment = "desafio-gb"             # AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
# SUB REDE EXTERNA. EXTERNA A
resource "aws_subnet" "sub_ext_prod_a" {
vpc_id     = aws_vpc.vpc_prod.id
  cidr_block = "10.110.11.0/24"
  availability_zone = "us-east-1a"
  
  tags = {
    Name = "sub_net_ext_prod_a"
    Envolroment = "desafio-gb"
  }
}