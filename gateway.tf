#INTERNET GATEWAY
resource "aws_internet_gateway" "igw_prd" {  # IGW_PRD É O NOME DO RECURSO PARA INTERNET GATEWAY QUE VAMOS UTILIZAR PARA DEIXAR NOSSA REDE EXTERNA PUBLICA PARA A INTERNET
  vpc_id = aws_vpc.vpc_prod.id               # AQUI VOCÊ DEFINE A QUAL VPC ESSA SUBREDE SERÁ ASSOCIADA. COLOQUE O NOME DA SUA VPC PRINCIPAL, NO MEU CASO, VPC_PROD.

  tags = {
    Name = "IGW_PRD"                         # AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
#NAT GATEWAY - Utilizado para permitir a navegação de internet das dub redes internas.
resource "aws_eip" "nat_gateway_eipa" {     # AQUI CRIAMOS UM IP ELÁSTICO DEDICADO NA REGIÃO A. ESSE IP SERÁ O DEFAULT DE SAIDA DE INTERNET DOS SEUS RECURSOS.
tags = {
    Name = "IP_ELÁSTICO_A"     
    Envolroment = "desafio-gb"                    # AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
resource "aws_eip" "nat_gateway_eipb" {     # AQUI CRIAMOS UM IP ELÁSTICO DEDICADO NA REGIÃO B. ESSE IP SERÁ O DEFAULT DE SAIDA DE INTERNET DOS SEUS RECURSOS.
tags = {
    Name = "IP_ELÁSTICO_B"     
    Envolroment = "desafio-gb"                    # AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
resource "aws_nat_gateway" "nat_gtw_a" {     # AQUI CRIAMOS O RECURSO NAT GATEWAY E DEFINIMOS O NOME COM UM ITEM APONTANDO PARA A REGIÃO A.
  allocation_id = aws_eip.nat_gateway_eipa.id   # AQUI NÓS AMARRAMOS O SERVIÇO AO IP PUBLICO CRIADO ACIMA. COMO É DE REGIÃO A, AMARRAMOS NO IP ELÁSTICO DA REGIÃO A.
  subnet_id   = aws_subnet.sub_ext_prod_a.id    # AQUI NÓS VAMOS VINCULAR O NATGATEWAY A SUBREDE EXTERNA. LEMBRE-SE QUE NÃO PODE SER AMARRADA A SUBREDE INTERNA.

  tags = {
    Name = "NAT_GTW_A"      
    Envolroment = "desafio-gb"                    # AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}
#OS MESMOS CONCEITOS DO NATGATEWAY DA REGIÃO A SE APLICAM AQUI, PORÉM NOTE QUE ESTAMOS FALANDO DE UMA REGIÃO B AGORA E PRECISAMOS NOS ATENTAR.
resource "aws_nat_gateway" "nat_gtw_b" {
  allocation_id = aws_eip.nat_gateway_eipb.id
  subnet_id   = aws_subnet.sub_ext_prod_b.id

  tags = {
    Name = "NAT_GTW_B"
    Envolroment = "desafio-gb"
  }
}