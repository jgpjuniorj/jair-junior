# CRIAÇÃO DE SECURITY GROUP QUE SERÁ AMARRADO AO ELB.
resource "aws_security_group" "internet_facing_sg" { #AQUI VOCÊ DEFINE O RECURSO E EM SEGUIDA, O NOME DO SECURITY GROUP
  name_prefix = "internet_facing_sg"                #AQUI VOCÊ DEFINE O NOME TAG DO SECURITY GROUP
  vpc_id = aws_vpc.vpc_prod.id                      #AQUI VOCÊ ASSOCIA SEU SG A VPC DO AMBIENTE
  
  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # COMO É UM BALANCE DE CARA PARA A INTERNET, PRECISAMOS MANTER ESSA REGRA ABERTA PARA O MUNDO
  }
}

resource "aws_lb" "internet-facing" {   #AQUI VOCÊ DEFINE O RECURSO E O NOME DO ELB
  name               = "internet-facing"  #AQUI VOCÊ DEFINE O TAG NAME DO RECURSO
  internal           = false              #AQUI DEFINIMOS QUE NÃO SERÁ DE TRÁFEGO INTERNO
  load_balancer_type = "application"      #AQUI VOCÊ DEFINE O TIPO DE ELB QUE SERÁ CRIADO
  security_groups   = [aws_security_group.internet_facing_sg.id]    #AQUI VOCÊ ASSOCIA O SG CRIADO ACIMA AO ELB

  enable_deletion_protection = false  # ESSE ITEM HABILITA A PROTEÇÃO CONTRA EXCLUSÃO ACIDENTAL

  enable_http2       = true   #AQUI VOCÊ HABILITA O PROTOCOLO HTTP2
  enable_cross_zone_load_balancing = true #AQUI VOCÊ HABILITA A ALTA DISPONIBILIDADE ENTRE ZONAS AWS

  subnets = [aws_subnet.sub_ext_prod_a.id, aws_subnet.sub_ext_prod_b.id]  #AQUI VOCÊ ADICIONA SUAS SUB REDES ZONAS A E B. COMO É PARA A INTERNET, PRECISA ADICIONAR AS DUAS SUBREDES PÚBLICAS.
}