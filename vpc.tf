resource "aws_vpc" "vpc_prod" {        #vpc_prod É O NOME QUE VOCÊ DEFINE PARA A SUA REDE, ESCOLHA UM NOME DE SUA PREFERÊNCIA.
    cidr_block = "10.110.0.0/16"       #AQUI VOCÊ DEFINE O BLOCO DE IPS QUE UTILIZARÁ EM SUA REDE. IDEAL SER /16 PELA QUANTIDADE DE HOSTS.
    enable_dns_hostnames = true        #AQUI VOCÊ HABILITA A RESOLUÇÃO DE NOMES DNS PARA A SUA VPC.
     tags = {
    Name = "VPC desafio" 
    Envolroment = "desafio-gb"           #AQUI VOCÊ DEFINE A TAG NAME QUE UTILIZARÁ NO RECURSO.
  }
}